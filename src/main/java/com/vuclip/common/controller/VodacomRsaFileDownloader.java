package com.vuclip.common.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.zip.GZIPInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

@Controller
public class VodacomRsaFileDownloader {

	private static final Logger logger = LoggerFactory.getLogger(VodacomRsaFileDownloader.class);
	static String USERNAME = "sftpvuclip";
	static String PASSWORD = "N;AwFSYZ22>g;hg=Q98]";
	static String HOST = "213.239.205.74";
	static int PORT = 22;
	//Files on remote server will be with .csv.gz extension(input file) 
	static String EXTENSION = ".csv.gz";
	//output file extension
	static String CSV_EXTENSION = ".csv";
	static String REMOTE_DIR = "/";
	//PEPS-991: remote file name is in all caps now
	static String REMOTE_SUB_FILE_NAME = "VUCLIP_VIDEO_DCB_SUBS_";
	static String REMOTE_INACTIVE_FILE_NAME = "VUCLIP_VIDEO_DCB_SUBS_INACTIVE_"; 
	//	static String LOCAL_DIR_TEMP = "/vsavs-renewal-files/downloaded/"; 
	static String LOCAL_DIR = "/vsavs-renewal-files/downloaded/";
	static String LOCAL_SUB_FILE_NAME_TEMP = "Vuclip_C2B-TEMP";
	static String LOCAL_SUB_FILE_NAME = "Vuclip_C2B-";
	static String LOCAL_INACTIVE_FILE_NAME_TEMP = "Vuclip_C2B_INACTIVE-TEMP"; 
	static String LOCAL_INACTIVE_FILE_NAME = "Vuclip_C2B_INACTIVE-";
	static final String CHARGING_METHOD_DEFAULT_VALUE = "RECURRING";

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String welcome(Model model) {
		logger.debug("welcome() is executed, value {}", "MEA");
		model.addAttribute("msg", "Hello File Downloder hu mai ");
		return "welcome";
	}

	@RequestMapping(value = "/vsavsFileDownloader", method = {RequestMethod.GET, RequestMethod.POST})
	public ResponseEntity<String> execute() throws IOException {
		if(downloadFiles()==1)
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failure");
		logger.debug("MODIFYING SUBSCRIBE BASE FILE...");
		if(makeChanges(LOCAL_SUB_FILE_NAME_TEMP,LOCAL_SUB_FILE_NAME)==1)
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failure");
		logger.debug("MODIFYING INACTIVE BASE FILE...");
		if(makeChanges(LOCAL_INACTIVE_FILE_NAME_TEMP,LOCAL_INACTIVE_FILE_NAME)==1)
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failure");
		logger.debug("Done!");
		return ResponseEntity.ok("Success");
	}

	private static int downloadFiles() {
		JSch jsch = new JSch();
		Session session = null;
		try {
			session = jsch.getSession(USERNAME, HOST, PORT);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(PASSWORD);
			logger.debug("Connecting...");
			session.connect();
			Channel channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp sftpChannel = (ChannelSftp) channel;
			sftpChannel.get(getRemoteFileName(REMOTE_SUB_FILE_NAME), getLocalFileName(LOCAL_SUB_FILE_NAME_TEMP));
			sftpChannel.get(getRemoteFileName(REMOTE_INACTIVE_FILE_NAME), getLocalFileName(LOCAL_INACTIVE_FILE_NAME_TEMP));

			sftpChannel.exit();
			logger.debug("Disonnecting...");
			session.disconnect();
		} catch (Exception e) {
			logger.error("Exception While File Processing..."+e); 
			return 1;
		}
		return 0;
	}

	private static int makeChanges(String file_name_temp, String file_name) {
		String readFile = getLocalFileName(file_name_temp);
		String WriteFile = getLocalWriteFileName(file_name);
		int i=0;
		BufferedReader br = null;
		//PEPS-991: Input file will be in .gz format
		GZIPInputStream gzipInput = null;
		BufferedWriter bw = null;
		String line = "";
		String cvsSplitBy = ";";
		String chargingMethod;

		try {
			gzipInput = new GZIPInputStream(new FileInputStream(readFile));
			br = new BufferedReader(new InputStreamReader(gzipInput));
			bw = new BufferedWriter(new FileWriter(WriteFile));
			bw.write("SUBSCRIPTION_ID,PACKAGE_ID,UPDATED_DATE,MSISDN,STATUS,START_DATE,END_DATE,CHARGING_METHOD");
			bw.newLine();

			//ignoring 1st line which is column headers
			line = br.readLine();
			if(line != null) {
				while ((line = br.readLine()) != null) {
					String[] words = line.split(cvsSplitBy);

					//VSAVS-284
					if(words.length == 8) {
						chargingMethod = words[7];
					} else {
						chargingMethod = CHARGING_METHOD_DEFAULT_VALUE;
					}

					bw.write(words[0] +","+words[1] +","+convertDate(words[2]) +","+words[3] +","+ words[4]+"," +convertDate(words[5]) +","+convertDate(words[6])+","+chargingMethod);
					bw.newLine();
				}
			} else {
				logger.debug("Empty file");
			}
		} catch (Exception e) {
			i=1;
			logger.error("Exception While File Processing..."+e);
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					i=1;
					logger.error("Exception While File Processing..."+e);
				}
			}
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					i=1;
					logger.error("Exception While File Processing..."+e);
				}
			}
		}
		return i;
	}

	private static String convertDate(String words) throws Exception {
		DateFormat originalFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ENGLISH);
		DateFormat targetFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		Date date=null;
		date = originalFormat.parse(words);
		return targetFormat.format(date);
	}

	private static String getLocalFileName(String local_sub_file_name_temp) {
		logger.debug("STORING AS ->"+LOCAL_DIR+appendTimeStamp(local_sub_file_name_temp)+EXTENSION);
		return LOCAL_DIR+appendTimeStamp(local_sub_file_name_temp)+EXTENSION;
	}

	private static String getLocalWriteFileName(String local_sub_file_name_temp) {
		logger.debug("STORING AS ->"+LOCAL_DIR+appendTimeStamp(local_sub_file_name_temp)+CSV_EXTENSION);
		return LOCAL_DIR+appendTimeStamp(local_sub_file_name_temp)+CSV_EXTENSION;
	}

	private static String getRemoteFileName(String remote_sub_file_name) {
		logger.debug("GETTING FILE ->"+REMOTE_DIR+appendTimeStamp(remote_sub_file_name)+EXTENSION);
		return appendTimeStamp(remote_sub_file_name)+EXTENSION;
	}

	private static String appendTimeStamp(String remoteFileName) {
		Date date=new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
		String timeStamp=simpleDateFormat.format(date);
		remoteFileName=remoteFileName+timeStamp;
		return remoteFileName;
	}

}